# Philosophey of Math

* [plato.stanford philosophy mathematics](https://plato.stanford.edu/entries/philosophy-mathematics/)

# Skeptical theism or agnosticism

* [skeptical theism](https://plato.stanford.edu/entries/skeptical-theism/)

# Ontological arguments

* [ontological arguments](https://plato.stanford.edu/entries/ontological-arguments/)

# Natural theology

* [natural theology](https://plato.stanford.edu/entries/natural-theology/)
