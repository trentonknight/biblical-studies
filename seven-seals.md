# The First Seal
[Rev 6:2](https://netbible.org/#!l/bible%3ARevelation%206%3A2-net_strongs2__notes%3ARevelation%206)
> 2. So I looked, and here came a white horse! The one who rode it had a bow, and he was given a crown, and as a conqueror he rode out to conquer. 

- [John Wesley's note](https://marvel.bible/index.php?text=cWesley&b=66&c=6&v=1) point to a Military conquest lead by [Trajan (/ˈtreɪdʒən/ TRAY-jən; born Marcus Ulpius Traianus](https://en.wikipedia.org/wiki/Trajan).
- [Joseph Benson](https://en.wikipedia.org/wiki/Joseph_Benson) notes within the [Joseph Benson's Commentary of the Old and New Testamen](https://marvel.bible/index.php?text=cBenson&b=66&c=6&v=1) points to Christ.
- [Josephh S. Exell](https://store.biblesoft.com/331-joseph-s-exell) notes with the [Biblical Illustrator](https://marvel.bible/index.php?text=cBI&b=66&c=6&v=1) point to Christ
- [The Cambridge Bible for Schools and Colleges](https://marvel.bible/index.php?text=cCBSC&b=66&c=6&v=1) points to a false Christ or woes before Christ's arrival.
- [Netnotes for the NET Bible](https://netbible.org/bible/Revelation+6#netNotesHolder) outline all three aforementioned belief of who the white horse rider is.
1. Christ
2. A Military Conquest
3. The Anti-Christ
- [The Constables Notes](https://netbible.org/bible/Revelation+6#constablesNotesHolder) List.
1. A Roman emperor
2. The Parthian invasion of the Roman Empire
3. The Messiah
4. The Antichrist
5. The Word of God
6. A personification of judgment
7. The victorious course of the gospel
8. Warfare in general
9. Triumphant militarism
10. The personification of ungodly movements.
and more. 

# The Second Seal
[Rev 6:3-4](https://netbible.org/#!l/bible%3ARevelation%206%3A2-net_strongs2__notes%3ARevelation%206)
> 3. Then when the Lamb opened the second seal, I heard the second living creature saying, “Come!” 
>  4. And another horse, fiery red, came out, and the one who rode it was granted permission to take peace from the earth so that people would butcher one another, and he was given a huge sword.


- [John Wesley's note](https://marvel.bible/index.php?text=cWesley&b=66&c=6&v=1) again points to Trajan[Trajan (/ˈtreɪdʒən/ TRAY-jən; born Marcus Ulpius Traianus](https://en.wikipedia.org/wiki/Trajan)
> To this horseman there was given a great sword; and he had much to do with it; for as soon as Trajan ascended the throne, peace was taken from the earth. Decebalus, king of Dacia, which lies westward from Patmos, put the Romans to no small trouble. The war lasted five years, and consumed abundance of men on both sides; yet was only a prelude to much other bloodshed, which followed for a long season. All this was signified by the great sword, which strikes those who are near, as the bow does those who are at a distance.

- [Joseph Benson](https://en.wikipedia.org/wiki/Joseph_Benson) notes within the [Joseph Benson's Commentary of the Old and New Testamen](https://marvel.bible/index.php?text=cBenson&b=66&c=6&v=1) points also to Trajan.
> When he opened the second seal, I heard the second living creature — Which was like an ox, and had his station toward the west; say, Come and see — As the former had done when the first seal was opened; and there went out another horse that was red — Seeming to betoken great slaughter and desolation by approaching wars: and to him that sat thereon was given to take peace from the earth — In the year 75, Vespasian had dedicated a temple to Peace: but after a time we hear no more of peace; all is full of war and bloodshed. According to Bishop Newton, this second period commences with Trajan, who came from the west, being a Spaniard by birth, and was the first foreigner who was elevated to the imperial throne. In his reign, and that of his successor, Adrian, there were horrid wars and slaughters, and especially between the rebellious Jews and Romans. Dion relates, that the Jews about Cyrene slew of the Romans and Greeks two hundred and twenty thousand men, with the most shocking circumstances of barbarity. In Egypt also, and in Cyprus, they committed the like barbarities, and there perished two hundred and forty thousand men more. But the Jews were subdued in their turn by the other generals and Lucius, sent against them by Trajan. Eusebius, writing of the same time, says, that the Jews, inflamed, as it were, by some violent and seditious spirit, in the first conflict gained a victory over the Gentiles, who, flying to Alexandria, took and killed the Jews in the city. The emperor sent Marius Turbo against them, with great forces by sea and land, who, in many battles, slew many myriads of the Jews. The emperor also, suspecting that they might make the like commotions in Mesopotamia, ordered Lucius Quietus to expel them out of the province, who, marching against them, slew a very great multitude of them there. Orosius, treating of the same time, says, that the Jews, with an incredible commotion, made wild, as it were, with rage, rose at once in different parts of the earth. For throughout all Libya they waged the fiercest wars against the inhabitants, and the country was almost desolated. Egypt also, Cyrene, and Thebais they disturbed with cruel seditions. But in Alexandria they were overcome in battle. In Mesopotamia also war was made upon the rebellious Jews by the command of the emperor. So that many thousands of them were destroyed with vast slaughter. They utterly destroyed Salamis, a city of Cyprus, having first murdered all the inhabitants. These things were transacted in the reign of Trajan; and in the reign of Adrian was their great rebellion, under their false Messiah Barchochab, and their final dispersion, after fifty of their strongest castles, and nine hundred and eighty-five of their best towns had been demolished, and after five hundred and eighty thousand men had been slain by the sword, besides an infinite number who had perished by famine and sickness, and other casualties; with great loss and slaughter too of the Romans, insomuch that the emperor forbore the usual salutations in his letters to the senate. Here was another illustrious triumph of Christ over his enemies; and the Jews and the Romans, both the persecutors of the Christians, were remarkably made the dreadful executioners of divine vengeance upon one another. The great sword and red horse are expressive emblems of this slaughtering and bloody period, and the proclamation for slaughter is fitly made by a creature like an ox, that is destined for slaughter. This period continued during the reigns of Trajan and his successors, by blood or adoption, about ninety-five years.

- [Josephh S. Exell](https://store.biblesoft.com/331-joseph-s-exell) notes with the [Biblical Illustrator](https://marvel.bible/index.php?text=cBI&b=66&c=6&v=1) 
- [The Cambridge Bible for Schools and Colleges](https://marvel.bible/index.php?text=cCBSC&b=66&c=6&v=1) 
- [Netnotes for the NET Bible](https://netbible.org/bible/Revelation+6#netNotesHolder) 
- [The Constables Notes](https://netbible.org/bible/Revelation+6#constablesNotesHolder) 

## Christian Eschatology
### The study of End Things or Last Things
[Review the Comparason of Futurist, Preterist and Historicist views on specific Last Things events.](https://en.wikipedia.org/wiki/Christian_eschatology)
## Timeline Images
Two of the images below were drawn by Clarence Larkin within his book [Dispensational Truth](https://www.preservedwords.com/disptruth/chap23.html)
- [Daniel and Revelation Compared](https://www.preservedwords.com/disptruth/images/23-compared.gif)
- [Daniel's Seventieth Week](https://so4j.com/images/daniels-seventieth-week-tribulation-end-times-chart.gif)
- [Gods Prophetic Timeline](http://www.lorigrimmett.com/wp-content/uploads/2017/09/End-Times-Gods-Prophetic-Timeline-07-WEB.jpg)
